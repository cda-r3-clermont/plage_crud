package com.humanbooster.crud.plage;

import com.humanbooster.crud.plage.models.Plage;
import com.humanbooster.crud.plage.services.PlageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class DataCreatorApplication {
    private Logger logger = LoggerFactory.getLogger(DataCreatorApplication.class);


    public static void main(String[] args){
        SpringApplication.run(DataCreatorApplication.class);
    }

    @Bean
    public CommandLineRunner dataLoader(PlageService plageService){
        return args -> {
            if(plageService.findByNom("Clermont Plage").isEmpty()){
                logger.info("Création d'une plage !");
                Plage plage = new Plage("Clermont Plage", "Place de Jaude",
                        "1", "63000", "Clermont-Ferrand", "aureliendelorme1@gmail.com",
                        "https://www.chu-clermontferrand.fr/sites/default/files/media/2021-03/Angelus%20YODASON%20%28CC%20BY%202.0%29.jpg");
                plageService.addPlage(plage);
                logger.info("La plage Clermont plage est ajoutée !");
            }

            if(plageService.findByNom("La plage de Jean Blanc").isEmpty()){
                logger.info("Création d'une plage !");
                Plage plage = new Plage("La plage de Jean Blanc", "Avenue du professeur Paul Emile Leroux",
                        "1", "83000", "Le Lavandou", "aureliendelorme1@gmail.com",
                        "https://dynamic-media-cdn.tripadvisor.com/media/photo-o/13/51/d9/ea/20180617-0934561-largejpg.jpg?w=1200&h=1200&s=1");

                plageService.addPlage(plage);

                logger.info("La plage de Jean Blanc est ajoutée !");
            }

            if(plageService.findByNom("Plage d'Etretat").isEmpty()){
                logger.info("Création d'une plage !");
                Plage plage = new Plage("Plage d'Etretat", "Place Victor Hugo",
                        "1", "76790", "Etretat", "aureliendelorme1@gmail.com",
                        "https://media.routard.com/image/42/3/etretat.1581423.w630.jpg");

                plageService.addPlage(plage);

                logger.info("La plage d'Etretat est ajoutée !");
            }

            if(plageService.findByNom("Plage de l'Ecluse").isEmpty()){
                logger.info("Création d'une plage !");
                Plage plage = new Plage("Plage de l'Ecluse", "Boulevard Wilson",
                        "1", "35800", "Dinard", "aureliendelorme1@gmail.com",
                        "https://cdt35.media.tourinsoft.eu/upload/Plage-de-l-Ecluse_4.jpg");

                plageService.addPlage(plage);

                logger.info("La plage de l'exluse est ajoutée !");
            }

            if(plageService.findByNom("Plage de la Grande Salinette").isEmpty()){
                logger.info("Plage de la Grande Salinette");
                Plage plage = new Plage("Plage de la Grande Salinette", "Boulevard du Bechay",
                        "1", "35800", "Saint-Briac-sur-Mer", "aureliendelorme1@gmail.com",
                        "https://cdt35.media.tourinsoft.eu/upload/Plage-de-la-Grande-Salinette_2.jpg");

                plageService.addPlage(plage);

                logger.info("La plage de l'exluse est ajoutée !");
            }


            if(plageService.findByNom("Criques de Porteils").isEmpty()){
                logger.info("Criques de Porteils");
                Plage plage = new Plage("Criques de Porteils", "RD114, Rte de Collioure",
                        "1", "66700", "Argelès-sur-Mer", "aureliendelorme1@gmail.com",
                        "https://dynamic-media-cdn.tripadvisor.com/media/photo-o/1c/9e/f2/e5/camping-les-criques-de.jpg?w=700&h=-1&s=1");

                plageService.addPlage(plage);

                logger.info("La plage de l'exluse est ajoutée !");
            }



        };
    }
}
