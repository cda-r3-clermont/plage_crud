package com.humanbooster.crud.plage;

import com.humanbooster.crud.plage.config.UploadImageConfig;
import com.humanbooster.crud.plage.services.UploadImageService;
import jakarta.annotation.Resource;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PlageApplication implements CommandLineRunner {

	@Resource
	UploadImageService uploadImageService;

	public static void main(String[] args) {
		SpringApplication.run(PlageApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		this.uploadImageService.init();
	}
}
