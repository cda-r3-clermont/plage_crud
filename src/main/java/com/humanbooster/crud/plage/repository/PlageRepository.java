package com.humanbooster.crud.plage.repository;

import com.humanbooster.crud.plage.models.Plage;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PlageRepository extends CrudRepository<Plage, Long> {
     @Override
     List<Plage> findAll();

     @Query(value = "FROM Plage ")
     Page<Plage> paginateResult(Pageable pageable);

     @Query(value = "DELETE FROM Plage")
     void removeAll();

     @Query(value = "FROM Plage p ORDER BY p.nom asc ")
     Page<Plage> pagePagination(Pageable page);

     List<Plage> findByNomContainingOrStreetContaining(String nom, String street);

     List<Plage> findByNom(String nom);
}



