package com.humanbooster.crud.plage.controllers;

import com.humanbooster.crud.plage.enums.Department;
import com.humanbooster.crud.plage.exceptions.WrongFileTypeException;
import com.humanbooster.crud.plage.forms.SearchForm;
import com.humanbooster.crud.plage.models.Plage;
import com.humanbooster.crud.plage.services.PlageService;
import com.humanbooster.crud.plage.services.UploadImageService;
import jakarta.validation.Valid;
import jakarta.websocket.server.PathParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.ModelAndView;

import java.io.IOException;
import java.util.List;

@Controller
@RequestMapping(path = "/plages")
public class PlageController {

    @Autowired
    private PlageService plageService;

    @Autowired
    UploadImageService uploadImageService;

    @RequestMapping("/list")
    public ModelAndView getAll(@RequestParam(required = false) String page){
        if(page == null){
            page = "1";
        }

        int pageNumber = Integer.valueOf(page);

        ModelAndView mv = new ModelAndView("plages/list");

        Page<Plage> plages = this.plageService.paginatePage(10, pageNumber-1);

        mv.addObject("plages", plages);
        mv.addObject("pageNumber", (String) page);

        return mv;
    }

    @RequestMapping("/{plage}")
    public ModelAndView getOne(@PathVariable(required = false) Plage plage){
        if(plage == null){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Plage inexistante");
        }
        ModelAndView mv = new ModelAndView("plages/one");
        mv.addObject("plage", plage);
        return mv;
    }

    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public ModelAndView add(){
        Plage plage = new Plage();
        ModelAndView mv = new ModelAndView("plages/form");
        mv.addObject("plage", plage);
        return mv;
    }

    @RequestMapping(value = "/edit/{plage}", method = RequestMethod.GET)
    public ModelAndView edit(@PathVariable(required = false) Plage plage){
        if(plage == null){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Plage inexistante");
        }
        ModelAndView mv = new ModelAndView("plages/form");
        mv.addObject("plage", plage);
        return mv;
    }


    @RequestMapping(value = "/delete/{plage}", method = RequestMethod.GET)
    public String delete(@PathVariable(required = false) Plage plage){
        if(plage == null){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Plage inexistante");
        }

        this.plageService.remove(plage);
        return "redirect:/plages/list";
    }



    @RequestMapping(value = "/edit/{plage}", method = RequestMethod.POST)
    public String editSubmit(@Valid Plage plage, BindingResult bindingResult){
        if(plage == null){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Plage inexistante");
        }

        if(bindingResult.hasErrors()) {
            return "plages/form";
        } else {
            this.plageService.save(plage);
            return "redirect:/plages/list";
        }
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String formAdd(@Valid Plage plage, BindingResult bindingResult,
                          @RequestParam("uploadImage") MultipartFile uploadImage,
                          Model model) throws IOException {
        if (plage == null) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, "Plage introuvable"
            );
        }
        if(bindingResult.hasErrors()) {
            return "plages/form";
        } else {
            try {
                plage.setImage(this.uploadImageService.upload(uploadImage));

            } catch (WrongFileTypeException e){
                model.addAttribute("uploadError", "Nous n'acceptons pas ce fichier ...");
                return "plages/form";
            }

            this.plageService.save(plage);
            return "redirect:/plages/list";
        }
    }

    @RequestMapping(value = "/search", method = RequestMethod.GET)
    public ModelAndView searchForm(){
        ModelAndView mv = new ModelAndView("plages/search-engine");

        List<String> departmentName = Department.getList(Department.DEPARTMENT_NAME, false);
        List<String> departmentNumero = Department.getList(Department.DEPARTMENT_NUMERO, false);

        List<Plage> plages = this.plageService.getAll();

        mv.addObject("form", new SearchForm());
        mv.addObject("departmentName", departmentName);
        mv.addObject("departmentNumero", departmentNumero);
        mv.addObject("plages", plages);

        return mv;
    }

    @RequestMapping(value = "/search", method = RequestMethod.POST)
    public ModelAndView searchFormSubmit(@Valid SearchForm searchForm, BindingResult bindingResult){

        ModelAndView mv = new ModelAndView("plages/search-engine");

        List<Plage> plages = this.plageService.searchEngine(searchForm);
        mv.addObject("plages", plages);
        mv.addObject("form", searchForm);

        List<String> departmentName = Department.getList(Department.DEPARTMENT_NAME, false);
        List<String> departmentNumero = Department.getList(Department.DEPARTMENT_NUMERO, false);

        mv.addObject("departmentName", departmentName);
        mv.addObject("departmentNumero", departmentNumero);

        return mv;
    }

}
