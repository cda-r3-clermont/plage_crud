package com.humanbooster.crud.plage.config;

import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.List;

@Configuration
public class UploadImageConfig {
    private String location = "uploads";

    private List<String> allowedFormat = new ArrayList<String>();

    public UploadImageConfig() {
        this.allowedFormat.add("image/jpeg");
        this.allowedFormat.add("image/png");
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public List<String> getAllowedFormat() {
        return allowedFormat;
    }

    public void setAllowedFormat(List<String> allowedFormat) {
        this.allowedFormat = allowedFormat;
    }
}
