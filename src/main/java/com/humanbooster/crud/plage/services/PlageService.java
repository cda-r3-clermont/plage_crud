package com.humanbooster.crud.plage.services;

import com.humanbooster.crud.plage.forms.SearchForm;
import com.humanbooster.crud.plage.models.Plage;
import com.humanbooster.crud.plage.repository.PlageCriteriaRepository;
import com.humanbooster.crud.plage.repository.PlageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PlageService {

    @Autowired
    private PlageRepository plageRepository;

    @Autowired
    private PlageCriteriaRepository plageCriteriaRepository;

    public List<Plage> getAll(){
        return this.plageRepository.findAll();
    }

    public void addPlage(Plage plage){
         this.plageRepository.save(plage);
    }

    public void removeData(){
        this.plageRepository.removeAll();
    }

    public List<Plage> findByNom(String nom){
        return this.plageRepository.findByNom(nom);
    }

    public void save(Plage plage){
        this.plageRepository.save(plage);
    }

    public void remove(Plage plage){
        this.plageRepository.delete(plage);
    }

    public Page<Plage> paginatePage(int nbResult, int page){
        Pageable pageable = PageRequest.of(page, nbResult);
        Page<Plage> plagePaginated = this.plageRepository.pagePagination(pageable);
        return plagePaginated;
    }

    public List<Plage> searchEngine(SearchForm searchForm){
       return this.plageCriteriaRepository.searchEnginePlage(searchForm);
    }



}
