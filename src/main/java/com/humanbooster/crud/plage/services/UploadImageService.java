package com.humanbooster.crud.plage.services;

import com.humanbooster.crud.plage.config.UploadImageConfig;
import com.humanbooster.crud.plage.exceptions.WrongFileTypeException;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.List;
import java.util.UUID;

@Service
public class UploadImageService {
    private final Path rootLocation;

    private String rootFolder;

    private List<String> allowedImages;

    public UploadImageService(UploadImageConfig config) {
        this.allowedImages = config.getAllowedFormat();
        this.rootLocation = Paths.get("src/main/resources/static/"+ config.getLocation());
        this.rootFolder = config.getLocation();
    }

    public String upload(MultipartFile file) throws WrongFileTypeException, IOException {
        if(!this.allowedImages.contains(file.getContentType())){
          throw new WrongFileTypeException();
        }

        String filePath = UUID.randomUUID() + "-"+file.getOriginalFilename();

        Path destination = this.rootLocation.resolve(Paths.get(filePath))
                .normalize()
                .toAbsolutePath();

        Files.copy(file.getInputStream(), destination, StandardCopyOption.REPLACE_EXISTING);

        return "/"+rootFolder+"/"+filePath;
    }

    public void init() throws IOException {
        Files.createDirectories(rootLocation);
    }
}
